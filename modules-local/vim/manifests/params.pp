class vim::params {

  $user    = $::id
  $group   = $::gid

  case $::osfamily {
      'RedHat' : {
          $home    = hiera('user::home', '/home')
      }
      default : {
          fail("${module_name} not supported on ${::osfamily}")
      }
  }

}
