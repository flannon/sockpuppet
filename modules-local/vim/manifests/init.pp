class vim(

    $home    = $vim::params::home,
    $user    = $vim::params::user,
    $group   = $vim::params::group,

) inherits vim::params {

  #$user  = $::id
  #$group = $::gid

  file { "${home}/${user}/.vimrc":
    ensure  => file,
    source  => "puppet:///modules/vim/vimrc",
    owner   => $user,
    group   => $group,
  }

  # make directory for vim setup
  file { "${home}/${user}/.vim":
    ensure  => directory,
    owner  => $user,
    group  => $group,
  }
  file { "${home}/${user}/.vim-setup.tar.gz":
    ensure  => present,
    mode    => '0755',
    source  => "puppet:///modules/vim/vim-setup.tar.gz",
  }
  exec {'unpack vim-setup':
    cwd     => "${home}/${user}/.vim",
    command => "/bin/tar zxf ${home}/${user}/.vim-setup.tar.gz",
    creates => "${home}/${user}/.vim/autoload",
    require => File["${home}/${user}/.vim", "${home}/${user}/.vim-setup.tar.gz"],
  }
}
