# Class: sockpuppet
# ===========================
#
# Full description of class sockpuppet here.
#
# Parameters
# ----------
#
# Document parameters here.
#
# * `sample parameter`
# Explanation of what this parameter affects and what it defaults to.
# e.g. "Specify one or more upstream ntp servers as an array."
#
# Variables
# ----------
#
# Here you should define a list of variables that this module would require.
#
# * `sample variable`
#  Explanation of how this variable affects the function of this class and if
#  it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#  External Node Classifier as a comma separated list of hostnames." (Note,
#  global variables should be avoided in favor of class parameters as
#  of Puppet 2.6.)
#
# Examples
# --------
#
# @example
#    class { 'sockpuppet':
#      servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#    }
#
# Authors
# -------
#
# Flannon Jackson (flannon@nyu.edu)
#
# Copyright
# ---------
#
# Copyright 2016 Your name here, unless otherwise noted.
#
class sockpuppet::install(
  $home = $sockpuppet::params::home,
  $confdir = $sockpuppet::params::confdir,
  $user = $sockpuppet::params::user,
  $group = $sockpuppet::params::group,
) inherits sockpuppet::params {
  case $user {
    'root':  {
      fail("${module_name} can not be run as root.")
    }
    default: {
      file { "${home}/${user}/bin":
        ensure => 'directory',
        owner  => $user,
        group  => $group,
        mode   => '0755',
      }
      file { "${home}/${user}/bin/sockpuppet.sh":
        ensure  => present,
        owner   => $user,
        group   => $group,
        mode    => '0755',
        content  => template('sockpuppet/sockpuppet.sh.erb'),
        require => File["${home}/${user}/bin"],
      }
      file { "${home}/${user}/bin/sockpuppet":
        ensure  => 'link',
        target  => "${home}/${user}/bin/sockpuppet.sh",
        require => File["${home}/${user}/bin/sockpuppet.sh"]
      }
      file { "${confdir}/var":
        ensure  => 'directory',
        owner   => $user,
        group   => $group,
        mode    => '0755',
      }
      file { "${confdir}/var/run":
        ensure  => 'directory',
        owner   => $user,
        group   => $group,
        mode    => '0755',
        require => File["${confdir}/var"],
      }
    } # End default
  } # End of case $::id
} # end
