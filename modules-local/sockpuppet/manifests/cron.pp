# Class: sockpuppet::cron
# ===========================
#
# sockpuppet::cron makes sockpuppet emulate puppet agent by
# doing regular puppet runs.  sockpuppet::cron loads two cron
# jobs: one applies the default.pp manifest and theother applies 
# the sockpuppet.pp manifest
#
#
#
#
# Authors
# -------
#
# Author Name <flannon@nyu.edu>
#
#
class sockpuppet::cron(
  $home  = $sockpuppet::params::home,
  $user  = $sockpuppet::params::user,
  $group = $sockpuppet::params::group,
) inherits sockpuppet::params {
  # Add cron job to apply sockpuppet.pp and default.pp manifests
  # For the cron job sockpuppet is invoke with a control script
  # in order to apply a random start time offset of 5 minutes
  # in an attempt to not clobber the puppet deamon with  
  # multiple requests
  case $user {
    'root':  {
      fail("${module_name} can not be run as root.")
    }
    default: {
        #
        # Apply the sockpuppet.pp manifest
        #
        cron { 'sockpuppet-cron.sh' :
            command  => "${home}/${user}/bin/sockpuppet-cron.sh",
            user    => "$user",
            #hour   => '*/1',
            #minute => '*/5',
            minute  => '10-59/25',
            require => File ["${home}/${user}/bin/sockpuppet-cron.sh"],
        }
        file { "${home}/${user}/bin/sockpuppet-cron.sh":
            ensure   => present,
            owner    => $user,
            group    => $group,
            mode     => '0755',
            content  => template('sockpuppet/sockpuppet-cron.sh.erb'),
            require  => File["${home}/${user}/bin"],
        }
    } # End default
  } # End of case $::id
} # end
