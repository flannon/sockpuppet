# Class: sockpuppet::config
# ===========================
#
# Full description of class sockpuppet::hiera here.
# sockpuppet::hiera manages hiera.yaml and hieradata for sockpuppet
#
# Examples
# --------
#
# @example
#    include sockpuppet::config
#
# Authors
# -------
#
# Author Name flannon@nyu.edu
#
# Copyright
# ---------
#
# Copyright 2016 Your name here, unless otherwise noted.
#
class sockpuppet::config(
  $home    = $sockpuppet::params::home,
  $confdir = $sockpuppet::params::confdir,
  $user    = $sockpuppet::params::user,
  $group   = $sockpuppet::params::group,
) inherits sockpuppet::params {
  case $user {
    'root':  {
      fail("${module_name} can not be run as root.")
    }
    default: {
      file { "/${confdir}/puppet.conf" :
        ensure  => present,
        owner   => $user,
        group   => $group,
        mode    => '0644',
        content => template('sockpuppet/puppet.conf.erb'),
      }
    } # End default
  } # Endof case $::id
} # end
