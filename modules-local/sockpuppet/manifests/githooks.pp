class sockpuppet::githooks(
    $home = $sockpuppet::params::home,
    $confdir = $sockpuppet::params::confdir,
    $user = $sockpuppet::params::user,
    $group = $sockpuppet::params::group,
) inherits sockpuppet::params {

    case $user {
        'root': {
            fail("${module_name} can not be run s root.")
        }
        default: {
            file { "${confdir}/.git/hooks/pre-commit" :
                ensure => present,
                owner  => $user,
                group  => $group,
                mode   => '0775',
                source => 'puppet:///modules/sockpuppet/pre-commit'
            }
            file { "${confdir}/.git/hooks/pre-commit-puppet-lint.sh" :
                ensure => present,
                owner  => $user,
                group  => $group,
                mode   => '0775',
                source => 'puppet:///modules/sockpuppet/pre-commit-puppet-lint.sh'
            }
            file { "${confdir}/.git/hooks/pre-commit-r10k-lint.sh" :
                ensure => present,
                owner  => $user,
                group  => $group,
                mode   => '0775',
                source => 'puppet:///modules/sockpuppet/pre-commit-r10k-lint.sh'
            }
        }
    }
}
