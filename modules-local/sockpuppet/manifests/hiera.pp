# Class: sockpuppet
# ===========================
#
# Full description of class sockpuppet::hiera here.
# sockpuppet::hiera manages hiera.yaml and hieradata for sockpuppet
#
# Examples
# --------
#
# @example
#    include sockpuppet::hiera
#    #class { 'sockpuppet::hiera':
#    #  servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#    #}
#
# Authors
# -------
#
# Author Name <author@domain.com>
#
# Copyright
# ---------
#
# Copyright 2016 Your name here, unless otherwise noted.
#
class sockpuppet::hiera(
  $home = $sockpuppet::params::home,
  $confdir = $sockpuppet::params::confdir,
  $user = $sockpuppet::params::user,
  $group = $sockpuppet::params::group,
) inherits sockpuppet::params {

  case $user {
    'root':  {
      fail("${module_name} can not be run as root.")
    }
    default: {
        file { "${confdir}/hiera.yaml" :
            ensure  => present,
            owner   => $user,
            group   => $group,
            mode    => '0644',
            content => template('sockpuppet/hiera.yaml.erb'),
        }
        file_line { 'sockpuppet::user' :
            ensure => present,
            path   => "${confdir}/hieradata/sockpuppet.yaml",
            line   => "sockpuppet::user: ${user}",
            match  => '^sockpuppet::user:',
        }

    } # End default
  } # Endof case $::id
} # end

