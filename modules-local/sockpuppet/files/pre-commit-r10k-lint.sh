#!/bin/sh
#
# This script assumes r10k is installed

echo "Performing syntax check on the r10k Puppetfile:"
r10k puppetfile check

if [[ $? -ne 0 ]]; then
    echo "Syntax errors in Puppetfile. To validate Puppetfile run "
    echo "   r10k puppetfile check."
    exit 1
else
    echo "Loading remote modules..."
    r10k puppetfile install
fi    
