# Class: cron_examples
# ===========================
#
# The value for $user is stored in the params.pp class
# which gets it from a hiera lookup on $confdir/hieradata/sockpuppet.pp
class cron_examples(
  $user   = $::id,    # $::id is a top level variable supplied by facter
  $group  = $::gid,   # $::gid is a top level variable supplied by facter
  $home   = hiera('sockpuppet::home', '/home')
)  {

  #
  # $state can be used to toggle the cron resources below.
  # The value of $state is defined $confdir/hieradata/common.yaml
  # in the key "cron_eamples::state", or as the default value 'present'.
  # To toggle the state of all cron resources listed below edit
  # $confdir/hieradata/common.yaml and change the value for
  # #cron_examples::state:" from "present" to absent.
  # Then run sockpuppet, "sockpuppet -a default.pp".
  $state = hiera('cron_examples::state', 'present')
  #
  # write log files to ~/tmp
  #
  file { "${home}/${user}/tmp" :
    ensure => directory,
    owner   => $user,
    group  => $group,
    mode   => '0755'
  }
  # Starting at 12 minutes after the hour run the job
  # every 25 minutes
  cron { 'offset-timer' :
    ensure  => $state,
    command => "/bin/date >> ${home}/${user}/tmp/offset.timer.log",
    user    => $user,
    minute  => '12-59/25',
    require => File["${home}/${user}/tmp"],
  }
  # Run a cron job once a minute, with $user from the hieradata
  cron { 'minute-timer' :
    ensure  => $state,
    command => "/bin/date >> ${home}/${user}/tmp/minute.timer.log",
    user    => $user,
    minute  => '*/1',
    require => File["${home}/${user}/tmp"],
  }
  #
  # Install a file and run it from cron once an hour.
  #
  #   First make sure ~/bin exits
  file { "${home}/${user}/bin" :
    ensure => $state,
    owner  => $user,
    group  => $group,
    mode   => '0755',
  }
  #   Then deliver the script from the modules "file" directory.
  file { "${home}/${user}/bin/mkfile.sh" :
    ensure => file,
    owner  => $user,
    group  => $group,
    mode   => '0755',
    # In the source attribute below, the apparent file uri is
    # actually a string which is internally interpreted by puppet.
    # The "files" directory is ommited from the attribut reference
    # and "modules" is explicitly stated, 
    # even if the file in question belongs to a module that
    # lives in "modules-local", or "sites" or any other location
    # properly referenced in $basemodulepath.
    source => 'puppet:///modules/cron_examples/mkfile.sh',
  }
  #   Now configure the cron resource.
  cron { 'once_an_hour' :
    ensure  => $state,
    command => "${home}/${user}/bin/mkfile.sh",
    user    => $user,
    hour    => '*/1',
    # Start the cron job only if the file resource
    # mkfile.sh has been applied
    require => File["${home}/${user}/bin/mkfile.sh"],
  }
}
