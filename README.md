## Sockpuppet

#### Table of Contents

1. [Overview](#overview)
2. [Pre-instalation setup](#pre-installation setup)
3. [Quick install guide](#quick install)
4. [Installation](#installation)
5. [Manifests](#manifests)
6. [Modules](#modules)


#### Overveiw

Sockpuppet is a framework for running puppet as a non privileged user.  It consists of an environment for running puppet; a puppet module for maintaining the sockpuppet environment; and a wrapper for puppet that lets you easily  apply manifests from the sockpuppet environment.


#### Pre-installation setup

You will need to have either [rvm](https://rvm.io/rvm/installi "rvm") or [rbenv](https://github.com/rbenv/rbenv "rbenv") setup and you'll need to install ruby 1.9.3.  You will also need to install gems for puppet, puppet-lint and r10k. Sockpuppet has been tested primarily with puppet 3.7.0 and 3.8.4, and briefly with 4.1.3.  Assuming you already using rvm these steps will ccomplete the pre-installation setup, 

    $ rvm install 1.9.3-p551
    $ rvm use 1.9.3
    $ gem install puppet -v 3.8.4
    $ gem install puppet-lint
    $ gem install rvm


#### Quick install

If you don't like to read instructions, and who does, these steps will get sockpuppet set up.  It won't do much yet, but the framework will be there. For details see the installation notes below.

    $ git clone ssh://git@v3.es.its.nyu.edu/fj5/sockpuppet ~/.puppet
    $ cd .puppet
    $ git checkout -b <username>-<projectname>
    $ cd ~/.puppet/configure
    $ ./configure.sh
    $ puppet apply --basemodulepath="~/.puppet/modules-local:~/.puppet/modules" ~/.puppet/manifests/sockpuppet.pp
    $ sockpuppet -a default.pp

>*Note:* The sockpuppet wrapper script will be installed in your bin directory.  If it doesn't already exist sockpuppet will create ~/bin, but you'll need to make sure that ~/bin is in your path.


#### Installation

To install sockpuppet clone the repo to ~/.puppet

    $ git clone ssh://git@v3.es.its.nyu.edu/fj5/sockpuppet ~/.puppet

Once you’ve cloned the repo check out a new branch and push it back to origin.  The branch naming convention is `<usename-hostname>`, or `<username>-<projectname>`.  For instance, I installed sockpuppet on tlon.dliockpuppet on nyu.edu so I did,

    $ git checkout –b fj5-tlon
    $ git push origin fj5-tlon

To get sockpuppet set up you’ll need to run the configure script, then run puppet apply against the sockpuppet manifest, after that you’ll be able to run sockpuppet to apply you’re manifests.

    $ cd ~/.puppet/configure
    $ ./configure.sh    

The configure script will install puppet.conf and hiera.yaml, and it will make some entries in hieradata/common.yaml and hieradata/sockpuppet.yaml that sockpuppet will need.  However, once you've applied the sockpuppet.pp manifest, sockpuppet will take care of maintaining ~/.puppet/puppet.conf and ~/.puppet/hiera.yaml.

    $ cd ~/.puppet
    $ puppet apply –basemodulepath=”~/.puppet/ \
        modules:~/.puppet/modules-local” manifests/sockpuppet.pp

When you run puppet apply for the first time it will create ~/bin, if it doesn't already exist, and install the puppet wrapper script sockpuppet.sh.  It will also make a link from sockpuppet to sockpuppet.sh, so you don’t have to type that annoying .sh everytime.  If ~/bin didn’t exist then it’s probably a safe assumption that ~/bin is not in your path, so you’ll need to add it to your path yourself.

Now you can apply the default manifest using sockpuppet,

    $ sockpuppet -a default.pp

At this point sockpuppet will have done absolutely nothing, but if it runs without errors it means sockpuppet is installed and you’re ready to go.  


#### Manifests

There are three manifest that get installed in ~/.puppet/manifests: default.pp, which is the main manifest that you'll use to configure the system; sockpuppet.pp, which you've already applied when you ran puppet apply; and vim.pp.sockpupet.pp, which has pathogen and vimcolors enabled, if you're in to that sort of thing.


#### sockpuppet.pp

 sockpuppet.pp has two include lines, though one is commented out to start with:

    include sockpuppet
    #include sockpuppet::cron
    
When you enable the "include sockpuppet::cron" line, sockpuppet will install a shell script in ~/bin that runs sockpuppet.pp mandifest and it makes an entry in your crontab to run the script about once every half hour. This line is initially commented out in case there are already entries in your crontab file.

It's possible to have both manual entries and puppet managed entries in your crontab file, puppet won't overwrite anything that's already there, but it's not recommended.  Puppet will add some comment lines and it's a bit of a mess to try and pick through by hand, so it's better to manage all of your crontab entries with sockpuppet. When you're ready to have sockpuppet manage your crontab file uncomment the "include sockpuppet::cron" line in sockppuet.pp and run sockpuppet,

    $ sockpuppet -a sockpuppet.pp


#### default.pp and crontab_examples

Initially your default.pp manifest has one line, which is commented out,

    #include cron_examples

cron_examples is a puppet module located in ~/.puppet/modules-local, that can be used as a template to write your own module.  When you uncomment it and run, 

    $ sockpuppet -a default.pp

it will add three entries to your crontab file and install a shell script in ~/bin, which is used by one of the cron jobs. When the default.pp manifest is applied it applies the puppet code in ~/.puppet/modules-local/cron_examples/manifests/init.pp. 

    
    cron { 'minute-timer' :
      ensure  => $state,
      command => '/bin/date >> /tmp/minute.timer.log',
      user    => $user,
      minute  => '*/1',
    }

The state of each resource managed by the cron_examples module is determined the value of the $state variable, which is supplied from by a hiera lookup.  If you look at ~/.puppet/hieradata/common.yaml you'll see the following entry,

    cron_examples::state: present

You can toggle the resources managed by cron_examples on and off by editing common.yaml and changing the value of cron_examples::state from "present" to "absent".  The next time that sockpuppet runs the cron entries that are managed by the cron_examples module will be removed, along with the shell script that it installed.  If you've enable sockpuppet::cron in ~/.puppet/manifests/sockpuppet.pp then sockpuppet will take care of removing the entries for cron_examples the next time it runs, or you can always run it manually to have the change take effect immediately by running,

    $ sockpuppet -a default.pp



