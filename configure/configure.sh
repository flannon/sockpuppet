#!/bin/bash
#
# The initial assumption is that sockpuppet is installed in ~/.puppet
# It works when installed in /home/user and /home/<example>/user
# If you install it anywhere else your mileage may vary.
# SELF= this program
# CONFDIR= ~/.puppet/configure
# BASEDIR= ~/.puppet
# TOPSDIRS= The path above the home directory
#
SELF=$(readlink -f $0)
CONFDIR=`dirname $SELF`
BASEDIR="$HOME/.puppet"
TOPDIRS=`dirname $HOME`
# Write puppet.con
erb $CONFDIR/templates/config.puppet.conf.erb > $BASEDIR/puppet.conf
# Write hiera.yaml
erb $CONFDIR/templates/config.hiera.yaml.erb > $BASEDIR/hiera.yaml
echo "Installed puppet.conf and hiera.yaml"
# Initials vars the require hiera.yaml to be in place
CONFDIRKEY='sockpuppet::confdir:'
SOCKPUPPETCONFDIR=`hiera -c $BASEDIR/hiera.yaml $CONFDIRKEY`
USERHOMEKEY='user::home:'
COMMONHOME=`hiera -c $BASEDIR/hiera.yaml $USERHOMEKEY`
SOCKPUPPETKEY='sockpuppet::home:'
SOCKPUPPETHOME=`hiera -c $BASEDIR/hiera.yaml $SOCKPUPPETKEY`
HEAD="$(git symbolic-ref HEAD 2>/dev/null)"
BRANCH=${HEAD##*/}
GITBRANCHKEY='sockpuppet::git-branch:'

if [[ $USER != 'root' ]]; then
    cd   $BASEDIR
    # If current branch is not 'master' use branchname as value
    # for  sockpuppet::git-branch.
    if [[ $BRANCH != 'master' ]]; then
        # sockpuppet::git-branch
        sed -i "s/$GITBRANCHKEY.*/$GITBRANCHKEY\ $BRANCH/" $BASEDIR/hieradata/sockpuppet.yaml
    else
        echo ''
        echo "Can't configure sockpuppet for branch $BRANCH"
        echo ''
        echo "  Usage: $0 <branch name>"
        echo ''
        exit 3
    fi
    #
    # If the value of common.yaml/user::homedir isn't consistent
    # with the top level elements of $HOME then
    # Add value of user::homedir to hieradata/common
    #   Note: the path validation with sed is a bit of a hack.
    #   I'm sure there's a cleaner way to do it, but it works for now.
    #
    # user::home
    if [[ $TOPDIRS != $USERHOMEKEY ]]; then
        sed -i "/$USERHOMEKEY/{s/.*/$USERHOMEKEY\ `echo /$TOPDIRS \
            | cut -d '/' --output-delimiter='\/' \
            -f2,3,4,5`/;:a;n;:ba;q}" $BASEDIR/hieradata/common.yaml
    fi
    # Add value of sockpuppet::home to hieradata/sockpuppet.yaml
    if [[ $TOPDIRS != $SOCKPUPPETHOME ]]; then
        sed -i "/$SOCKPUPPETKEY/{s/.*/$SOCKPUPPETKEY\ `echo /$TOPDIRS \
            | cut -d '/' --output-delimiter='\/' \
            -f2,3,4,5,6,7`/;:a;n;:ba;q}" $BASEDIR/hieradata/sockpuppet.yaml
    fi
    # Add value of sockpuppet::confdir
    if [[ $TOPDIRS != $SOCKPUPPETCONFDIR ]]; then
        sed -i "/$CONFDIRKEY/{s/.*/$CONFDIRKEY\ `echo /$BASEDIR \
            | cut -d '/' --output-delimiter='\/' \
            -f2,3,4,5,6,7`/;:a;n;:ba;q}" $BASEDIR/hieradata/sockpuppet.yaml
    fi
    if [[ ! -d $BASEDIR/configure/var/run/puppet ]]; then
       mkdir -p $BASEDIR/configure/var/run/puppet
    fi 

else
    echo "$0 can't be run as $USER"
    exit 1
fi

